'use strict';

/**
 * Error reporting
 */

const bugsnag = require('bugsnag');

bugsnag.register('0c2f82650bcd269d29068b58ccd625bf', {
  appVersion: require('./package.json').version
});


/**
 * Dependencies
 */

const serialize = require('syslog-serialize');
const udp = require('dgram');


/**
 * Initialization
 */

const HOST = process.env.HOSTNAME;
const APP = process.env.ROQET_APP;

const socket = udp.createSocket('udp4');


/**
 * Pipe stdin to logs.roqet.io
 */

process.stdin.on('data', function (data) {
  let message = data.toString().trim();

  send(message);

  console.log(message);
});


/**
 * Helpers
 */

function send (message) {
  let serialized = serialize({
    process: APP,
    pid: HOST,
    message: message
  });

  let data = new Buffer(serialized);
  
  socket.send(data, 0, data.length, 514, 'logs.roqet.io');
}
